package com.devcamp.rectanglerestapi.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.rectanglerestapi.models.Rectangle;

@RestController
@RequestMapping("/")
@CrossOrigin
public class RectangleRestAPI {
    @GetMapping("/rectangle-area")
    public double areaRec(@RequestParam(required = true, name = "length") float length, @RequestParam(required = true, name = "length") float width ){
        Rectangle rectangle = new Rectangle(length,width);
        return rectangle.getArea();
    }

    @GetMapping("/rectangle-perimeter")
    public double perimeterRec(@RequestParam(required = true, name = "length") float length, @RequestParam(required = true, name = "length") float width ){
        Rectangle rectangle = new Rectangle(length,width);
        return rectangle.getPerimeter();
    }

}
